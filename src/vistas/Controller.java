package vistas;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.sun.deploy.panel.IProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import logica.ConstruirDatagrama;
import modelo.Fragmento;

import javax.swing.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    @FXML
    private ImageView imgPlaneta;

    @FXML
    private TableView<Fragmento> tableResultados;

    @FXML
    private TableColumn<Fragmento, Integer> tableLongitud;

    @FXML
    private TableColumn<Fragmento, Integer> tableFlag1;

    @FXML
    private TableColumn<Fragmento, Integer> tableFlag2;

    @FXML
    private TableColumn<Fragmento, Integer> tableFlag3;

    @FXML
    private TableColumn<Fragmento, String> tableOffsetBinario;

    @FXML
    private TableColumn<Fragmento, Integer> tableOffsetDecimal;

    @FXML
    private TableColumn<Fragmento, String> tableHexadecimal;

    @FXML
    private JFXTextField txtMtu;

    @FXML
    private JFXTextField txtDatos;

    @FXML
    private JFXButton btnCalcular;

    @FXML
    private Label txtError1;

    @FXML
    private Label txtError2;

    private ConstruirDatagrama construirDatagrama;

    @FXML
    void onBtnCalcular(ActionEvent event) {

        if(!validarCampos()){

            txtError1.setVisible(false);
            txtError2.setVisible(false);

            tableResultados.getItems().clear();


            int tamañoDatos = Integer.parseInt(txtDatos.getText());
            int mtu = Integer.parseInt(txtMtu.getText());

            tableLongitud.setCellValueFactory(new PropertyValueFactory<>("tamanio"));
            tableFlag1.setCellValueFactory(new PropertyValueFactory<>("flag1"));
            tableFlag2.setCellValueFactory(new PropertyValueFactory<>("flag2"));
            tableFlag3.setCellValueFactory(new PropertyValueFactory<>("flag3"));
            tableOffsetDecimal.setCellValueFactory(new PropertyValueFactory<>("offsetDecimal"));
            tableOffsetBinario.setCellValueFactory(new PropertyValueFactory<>("offsetBinario"));
            tableHexadecimal.setCellValueFactory(new PropertyValueFactory<>("hex"));


            ArrayList<Fragmento> lista = construirDatagrama.fragmentar(tamañoDatos, mtu);
            System.out.println(lista.size());

            for (Fragmento fragmento : lista) {
                tableResultados.getItems().add(fragmento);
            }

            tableResultados.refresh();
        }

    }

    public boolean validarCampos (){
        boolean campos = false;

        tableResultados.getItems().clear();
        txtError1.setVisible(false);
        txtError2.setVisible(false);


        if(!txtDatos.getText().matches("[0-9]*")){
            campos = true;
            txtError1.setText("Solo se admiten datos numericos");
            txtError1.setVisible(true);
        }

        if(!txtMtu.getText().matches("[0-9]*")){
            campos = true;
            txtError2.setText("Solo se admiten datos numericos");
            txtError2.setVisible(true);
        }

        if(txtDatos.getText().toString().equals("")){
            campos = true;
            txtError1.setText("Por favor llene el campo de texto");
            txtError1.setVisible(true);
        }

        if(txtMtu.getText().toString().equals("")){
            campos = true;
            txtError2.setText("Por favor llene el campo de texto");
            txtError2.setVisible(true);

        }
        return campos;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }
}
