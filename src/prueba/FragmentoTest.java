package prueba;

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

import logica.ConstruirDatagrama;
import modelo.Fragmento;;

public class FragmentoTest {

	@Test
	public void testAssertEquals() {
		ArrayList<Fragmento> fragmentos = new ArrayList<>();

		Fragmento frag1 = new Fragmento();
		frag1.setTamanio(620);
		frag1.setFlag1(0);
		frag1.setFlag2(0);
		frag1.setFlag3(1);
		frag1.setOffsetDecimal(0);
		frag1.setOffsetBinario("0000000000000");
		frag1.setHex("0x2000");

		Fragmento frag2 = new Fragmento();
		frag2.setTamanio(620);
		frag2.setFlag1(0);
		frag2.setFlag2(0);
		frag2.setFlag3(1);
		frag2.setOffsetDecimal(75);
		frag2.setOffsetBinario("0000001001011");
		frag2.setHex("0x204b");

		Fragmento frag3 = new Fragmento();
		frag3.setTamanio(220);
		frag3.setFlag1(0);
		frag3.setFlag2(0);
		frag3.setFlag3(0);
		frag3.setOffsetDecimal(150);
		frag3.setOffsetBinario("0000010010110");
		frag3.setHex("0x0096");

		fragmentos.add(frag1);
		fragmentos.add(frag2);
		fragmentos.add(frag3);

		ArrayList<Fragmento> lista = ConstruirDatagrama.fragmentar(1400, 620);

		for (int i = 0; i < lista.size(); i++) {
			assertEquals(fragmentos.get(i).getTamanio(), lista.get(i).getTamanio());
			assertEquals(fragmentos.get(i).getFlag1(), lista.get(i).getFlag1());
			assertEquals(fragmentos.get(i).getFlag2(), lista.get(i).getFlag2());
			assertEquals(fragmentos.get(i).getFlag3(), lista.get(i).getFlag3());
			assertEquals(fragmentos.get(i).getHex(), lista.get(i).getHex());
			assertEquals(fragmentos.get(i).getOffsetBinario(), lista.get(i).getOffsetBinario());
			assertEquals(fragmentos.get(i).getOffsetDecimal(), lista.get(i).getOffsetDecimal());
		}
	}
	
	@Test
	public void testAssertNotEquals() {
		ArrayList<Fragmento> fragmentos = new ArrayList<>();

		Fragmento frag1 = new Fragmento();
		frag1.setTamanio(600);
		frag1.setFlag1(0);
		frag1.setFlag2(0);
		frag1.setFlag3(0);
		frag1.setOffsetDecimal(75);
		frag1.setOffsetBinario("0000000000011");
		frag1.setHex("0x20f0");

		Fragmento frag2 = new Fragmento();
		frag2.setTamanio(600);
		frag2.setFlag1(0);
		frag2.setFlag2(0);
		frag2.setFlag3(0);
		frag2.setOffsetDecimal(150);
		frag2.setOffsetBinario("0000001001000");
		frag2.setHex("0x200a");

		Fragmento frag3 = new Fragmento();
		frag3.setTamanio(700);
		frag3.setFlag1(0);
		frag3.setFlag2(0);
		frag3.setFlag3(1);
		frag3.setOffsetDecimal(0);
		frag3.setOffsetBinario("0000000000000");
		frag3.setHex("0x0000");

		fragmentos.add(frag1);
		fragmentos.add(frag2);
		fragmentos.add(frag3);

		ArrayList<Fragmento> lista = ConstruirDatagrama.fragmentar(1400, 500);

		for (int i = 0; i < lista.size(); i++) {
			assertNotEquals(fragmentos.get(i).getTamanio(), lista.get(i).getTamanio());
			assertNotEquals(fragmentos.get(i).getFlag3(), lista.get(i).getFlag3());
			assertNotEquals(fragmentos.get(i).getHex(), lista.get(i).getHex());
			assertNotEquals(fragmentos.get(i).getOffsetBinario(), lista.get(i).getOffsetBinario());
			assertNotEquals(fragmentos.get(i).getOffsetDecimal(), lista.get(i).getOffsetDecimal());
		}

	}

}
