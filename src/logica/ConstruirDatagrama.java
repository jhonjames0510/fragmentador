package logica;

import java.math.BigInteger;
import java.util.*;

import modelo.Fragmento;

import javax.swing.*;

public class ConstruirDatagrama {

    public static ArrayList<Fragmento> fragmentar(int tamanio, int mtu) {

        ArrayList<Fragmento> listaFragmentos = new ArrayList<>();
        int cabecera = 20;
        int desplazamiento = 0;

        if (validarCampos(tamanio, mtu)) {

            while (tamanio + cabecera > mtu) {

                Fragmento fragment = new Fragmento();

                fragment.setTamanio(mtu);
                fragment.setFlag1(0);
                fragment.setFlag2(0);
                fragment.setFlag3(1);
                fragment.setOffsetDecimal(desplazamiento / 8);

                String offSetBinario = completarBinario(Integer.toString(desplazamiento / 8, 2));

                String flags = "001";
                String hexBinario = flags + offSetBinario;

                String hex16bits = new BigInteger(hexBinario, 2).toString(16);

                fragment.setOffsetBinario(offSetBinario);
                fragment.setHex(completarHexa(hex16bits));

                listaFragmentos.add(fragment);

                desplazamiento += fragment.getTamanio() - cabecera;

                tamanio = tamanio - (mtu - cabecera);

            }

            if (tamanio > 0 || tamanio + cabecera <= mtu) {
                Fragmento fragment = new Fragmento();

                fragment.setTamanio(tamanio + cabecera);
                fragment.setFlag1(0);
                fragment.setFlag2(0);
                fragment.setFlag3(0);
                fragment.setOffsetDecimal(desplazamiento / 8);

                String offSetBinario = completarBinario(Integer.toString(desplazamiento / 8, 2));
                String flags = "000";
                String hexBinario = flags + offSetBinario;

                String hex16bits = new BigInteger(hexBinario, 2).toString(16);

                fragment.setOffsetBinario(offSetBinario);
                fragment.setHex(completarHexa(hex16bits));

                listaFragmentos.add(fragment);
            }

        } else {
            JOptionPane.showMessageDialog(null, "MTU o tamaño invalidos", "Error", JOptionPane.ERROR_MESSAGE);

        }
        return listaFragmentos;

    }

    public static boolean validarCampos(int tamanio, int mtu) {

        return (tamanio > 21 && ((mtu - 20) % 8 == 0));

    }

    public static String completarBinario(String binary) {

        while (binary.length() <= 12) {
            binary = "0" + binary;
        }

        return binary;

    }

    public static String completarHexa(String binary) {

        while (binary.length() <= 3) {
            binary = "0" + binary;
        }

        return "0x" + binary;

    }

    public static String toHex(int decimal) {
        int rem;
        String hex = "";
        char hexchars[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        while (decimal > 0) {
            rem = decimal % 16;
            hex = hexchars[rem] + hex;
            decimal = decimal / 16;
        }
        return hex;
    }

}
