package modelo;

public class Fragmento {

	private int tamanio;

	private int flag1;

	private int flag2;

	private int flag3;

	private int offsetDecimal;

	private String offsetBinario;

	private String hex;

	public Fragmento() {
		super();
	}

	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}

	public int getFlag1() {
		return flag1;
	}

	public void setFlag1(int flag1) {
		this.flag1 = flag1;
	}

	public int getFlag2() {
		return flag2;
	}

	public void setFlag2(int flag2) {
		this.flag2 = flag2;
	}

	public int getFlag3() {
		return flag3;
	}

	public void setFlag3(int flag3) {
		this.flag3 = flag3;
	}

	public int getOffsetDecimal() {
		return offsetDecimal;
	}

	public void setOffsetDecimal(int offsetDecimal) {
		this.offsetDecimal = offsetDecimal;
	}

	public String getOffsetBinario() {
		return offsetBinario;
	}

	public void setOffsetBinario(String offsetBinario) {
		this.offsetBinario = offsetBinario;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}

	@Override
	public String toString() {
		return "Fragmento [tama�o=" + tamanio + ", flag1=" + flag1 + ", flag2=" + flag2 + ", flag3=" + flag3
				+ ", offsetDecimal=" + offsetDecimal + ", offsetBinario=" + offsetBinario + ", hex=" + hex + "]";
	}

}
